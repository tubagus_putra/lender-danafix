const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: [
    './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
    './vendor/laravel/jetstream/**/*.blade.php',
    './storage/framework/views/*.php',
    './resources/views/**/*.blade.php',
    './resources/js/**/*.vue' 
  ],
  theme: {
    fontFamily: {
      'sans': ['"Montserrat"', 'Arial', 'sans-serif'],
    },
    colors: { 
      primary: '#6C4FC9', 
      secondary: '#ECC94B', 
      tertiary: '#00c8dc',  
      dark: '#464646', 
      light: '#F9F9F9', 
      gray: '#EFEFEF', 
      white: '#FFFFFF', 
    },
    backgroundColor: { 
      primary: '#6C4FC9', 
      secondary: '#ECC94B', 
      tertiary: '#00c8dc',
      light: '#f8f9fa', 
      gray: '#EFEFEF', 
      white: '#FFFFFF', 
    },
    container: {
      center: true,
      screens: { 
        sm: "640px", 
        md: "768px", 
        lg: "1024px", 
        xl: "1180px" 
      }
    },
    extend: {
      fontFamily: { 
        sans: ['Monstserrat', ...defaultTheme.fontFamily.sans] 
      },
      boxShadow: {
        dark: '0px 20px 80px rgba(0, 0, 0, 0.1)',
      },
    },
  },
  variants: {
    extend: { 
      opacity: ['disabled'] 
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')] 
};
